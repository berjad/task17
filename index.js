// function sayHello() {
//     console.log("Hello from node!");
// }

// sayHello();

const fs = require('fs');
const eol = require('os').EOL;

console.log(JSON.stringify(eol));


fs.readFile( __dirname + '/mytest.csv', 'utf-8', (err, contents) => {

    // console.log('Error: ', err);

    // console.log('File content: ',contents);   

    const rows = contents.split(eol);
    console.log(rows);
    
    const quotes = rows.map(row => {
        
        const cols = row.split(',')
        return {
            name: cols[0],
            quote1: cols[1],
            quote2: cols[2]
        };
       
        
    });
    console.log(quotes);

});


// setTimeout(function(){
//     console.log('Hello from a timeout!');
    
// }, 2000);

// console.log( __dirname );
// console.log( __filename );



