const fs = require('fs');
const readline = require('readline');
const os = require('os');
const http = require('http');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Get OS info
let getMem = os.totalmem() / 1024 / 1024 / 1024;
let mem = getMem.toFixed(0);
let getFreeMem = os.freemem() /1024 / 1024 / 1024;
let freeMem = getFreeMem.toFixed(0);
let cpu = os.cpus().length;
let arch = os.arch();
let platform = os.platform();
let release = os.release();
let getUser = os.userInfo();
let user = getUser.username;




const message = `
choose an option:
1. Read my package.json
2. Get my OS info
3. Start a HTTP server
------------------------
Type a number: `

rl.question(message, (answer) => {
    
    if (answer == 1) {
        fs.readFile( __dirname + '/package.json', 'utf-8', (err, contents) => {
            console.log(contents);
            rl.close();

        });
        
    } else if (answer == 2) {
        
        console.log(`Getting OS info...`);
        console.log(`SYSTEM MEMORY: ${mem} GB`);
        console.log(`FREE MEMORY: ${freeMem} GB`);
        console.log(`CPU CORES: ${cpu}`);
        console.log(`ARCH: ${arch}`);
        console.log(`PLATFORM: ${platform}`);
        console.log(`RELEASE: ${release}`);
        console.log(`USER: ${user}`);
 

    } else if (answer == 3) {
        
        console.log(`Starting HTTP server...`);
        console.log(`listening on port 3000...`);
        
        server = http.createServer((req, res) => {

            res.write('Hello World!');
            res.end();
    });
    
    server.listen(3000);

    } else {
        console.log('Invalid option!');
     
    }     
    rl.close();
    
});